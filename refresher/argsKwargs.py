# *ARGS
def my_meth(arg1, arg2):
    return arg1 + arg2

print(my_meth(1, 2))

def my_long_meth(arg1, arg2, arg3, arg4, arg5):
    return arg1 + arg2 + arg3 + arg4 + arg5

print(my_long_meth(1, 2, 3, 4, 5))

def my_list_addition(list_arg):
    return sum(list_arg)

print(my_list_addition([1, 3, 4, 5, 6]))

def addition_simplified(*args):
    return sum(args)

print(addition_simplified(1, 4, 4, 6, 10))


##
def what_are_kwargs(*args, **kwargs):
    print(args)
    print(kwargs)

print(what_are_kwargs())
    