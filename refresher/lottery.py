"""
Some misc work with classes and objects.
"""
lottery_player = {
        'name' : 'Jim',
        'numbers' : '(1, 24, 53, 67, 92)'
    }

# Create a Lottery Player class
class LotterPlayer(object):
    def __init__(self, name, numbers):
        self.name = name
        self.numbers = numbers

    def find_matches(self, winning_numbers):
        """ Compare against the winning numbers. Return the matches. """
        unique_nums = set(self.numbers)
        winning_numbers = set(winning_numbers)
        return unique_nums.intersection(winning_numbers)

player1 = LotterPlayer('Jim', (1, 24, 53, 67, 92))
print(player1.find_matches((1, 4, 67, 93, 53)))

# Create a Student Class
class Student(object):
    def __init__(self, name, school):
        self.name = name
        self.school = school
        self.marks = []

    def get_gpa(self):
        return sum(self.marks) / len(self.marks)

    @staticmethod
    def go_to_school():
        print('Im going to school!')

mary = Student('Mary', 'Franklin')
mary.marks = [56, 75, 85, 92, 96, 100]
print("{name}'s GPA is : {avg}".format(
        name=mary.name,
        avg=mary.get_gpa()
        )
    )

print(Student.go_to_school())


# Create a Store classes
class Store(object):
    def __init__(self, name):
        self.name = name
        self.items = []

    def add_item(self, name, price):
        item = {'item_name': name, 'item_price': price}
        self.items.append(item)

    def stock_price(self):
        prices = [item['item_price'] for item in self.items]
        total = sum(prices)
        #return a price rounded to the nearest cent.
        return round(total, 2)


jhw = Store("John's Hardware Store")
jhw.add_item('bolt', 12.99)
jhw.add_item('hammer', .039)
jhw.add_item('window', 100)
jhw.add_item('pvc pipe', 3.99)

print("The stores Stock Price is : {}".format(jhw.stock_price()))
