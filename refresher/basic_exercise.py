# Should_continue = True
# if should_continue:
#     print('Hello')

# known_people = ['John', 'Mark', 'Joe', 'Mary', 'Jane']
# 
# person = input('Enter the person you know: ')
# if person in known_people:
#     print('You know {}'.format(person))
# else:
#     print('You do not know {}'.format(person))


def who_do_you_know():
    # Ask the user for list of people they know
    # split the string into a list
    # Return that list
    known_people = input('Enter a list of the people you know, separarted by commas: ')
    people_list = [person.strip() for person in known_people.split(',')]
    return people_list

def ask_user():
    # Ask the user for a name
    # See if their name is in the list of people they know
    # print out that they know that person.
    person = input('Enter a name: ')
    if person in who_do_you_know():
        print('You know {}!'.format(person))
    else:
        print('You do not know {}!'.format(person))

ask_user()
