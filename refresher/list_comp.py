mylist = [1, 3, 5, 6, 2, 6, 8, 9]

comp1 = [x for x in range(1, 6)] #1, 2, 3, 4, 5
mult_comp = [x * 3 for x in range(1, 6)] #3, 6, 9, 12, 15

even_nums = [x for x in range(30) if x % 2 == 0]

people = ['Jim', " tammy", "WANDA"]
normalized_people = [x.strip().lower() for x in people]


