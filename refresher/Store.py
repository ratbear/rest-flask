class Store:
    def __init__(self, name):
        self.name = name
        self.items = []

    def add_item(self, name, price):
        self.items.append({
            'name': name,
            'price': price
        })

    def stock_price(self):
        total = 0
        for item in self.items:
            total += item['price']
        return total

    @classmethod
    def franchise(cls, store):
        # Return a new store, with the same name as the argument's name, plus " - franchise"
        name = "{} = franchise".format(store.name)
        return = cls(name)

    @staticmethod
    def store_details(store):
        # Return a string representing the argument
        # It should be in the format 'NAME, total stock price: TOTAL'
        return "{name}, total stock price: {total}".format(
            name=store.name,
            total=store.stock_price()
        )