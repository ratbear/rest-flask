class Student(object):
    def __init__(self, name, school):
        self.name = name
        self.school = school
        self.marks = []

    def average(self):
        return sum(self.marks) / len(self.marks)

    @classmethod
    def friend(cls, origin, friend_name, *args, **kwargs):
        """return new student with a new name but same school as self. 
        :param origin: (obj) Student
        :param friend_name: (str) Name of the friend.
        """
        return cls(friend_name, origin.school, *args, **kwargs)

class WorkingStudent(Student):
    def __init__(self, name, school, salary, job_title):
        # Call the init method from the parent class
        super().__init__(name, school)
        # Specify new attributes
        self.salary = salary
        self.job_title = job_title

# Create a working student
anna = Student('anna', 'oxford')
print(dir(anna))
print(anna.name)
print(anna.school)

# Create a friend who is not a working student
friend = WorkingStudent.friend(anna, 'Greg', 17.50, job_title='Engineer')
print(dir(friend))
print(friend.name)
print(friend.school)
print(friend.salary)
print(friend.job_title)