
students = [
        {'name': 'Jose', 'Major': 'Computing', 'grades': (90, 85, 91)},
        {'name': 'Jammie', 'Major': 'Computing', 'grades': (96, 79, 89)}
    ]

def avg_grade(data):
    grades = data['grades']
    return sum(grades) / len(grades)

def class_avg(studlist):
    class_grades = []
    for student in students:
        for grade in student['grades']:
            class_grades.append(grade)

    return sum(class_grades) / len(class_grades)

print(class_avg(students))
