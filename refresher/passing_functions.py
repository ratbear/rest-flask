def methodception(another):
    return another()

def add_two_numbers():
    return 35 + 77

def get_evens(x):
    return x % 2 == 0

print(methodception(add_two_numbers))

print(methodception(lambda: 35 + 77))

my_list = [13, 56, 77, 484]

# using lambda
print(list(filter(lambda x: (x % 2) == 0, my_list)))

#using a 'real' function
print(list(filter(get_evens, my_list)))

#using a list comprehension
print([x for x in my_list if (x % 2) == 0])