#Decorators are just functions that wrap around other functions
# They get called by using the '@' symbol and the fucntion that follows
# decorator call is passed an argument to the decorator. 

import functools

####
# without arguements
####

def my_decorator(func):
    @functools.wraps(func)
    def function_that_runs_fun():
        print("In the dcorator!")
        func() # The decorator should always call a another funtion
        print("After the decorator!")
    return function_that_runs_fun


@my_decorator
def my_function():
    print("I'm the function")

my_function()

####
# Complex Decorators: with arguments
####

def decorator_with_args(number):
    def my_decorator(func):
        @functools.wraps(func)
        def function_that_runs_func(*args, **kwargs):
            print("In the decorator")
            func(*args, **kwargs)
            print("After the decorator")
        return function_that_runs_func
    return my_decorator

@decorator_with_args(56)
def my_function_too():
    print("Hello")

my_function_too()