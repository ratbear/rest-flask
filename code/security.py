from werkzeug.security import safe_str_cmp
from user import User

users = [
    User(1, 'bob', 'abcd'),
    User(2, 'bill', '1234')
]

#create mappings for both user name and user id to avoid excessive iteration
username_mapping = {u.username: u for u in users} #username: User Object
userid_mapping = {u.id: u for u in users} # user id: User Object

def authenticate(username, password):
    user = username_mapping.get(username, None)
    if user and safe_str_cmp(user.password, password):
        return user

def identity(payload):
    user_id = payload['identity']
    return userid_mapping.get(user_id, None)