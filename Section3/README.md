This was the first exercise that gets into API creation
and goes over some basic explaination of how to create a
store and add some inventory it to it. The database used 
is simpley a python dictionary. POSTman is also introduced
as a tool for testing and evaluating your APIs.

The only python package required for this exercise was
flask.

$ pip3.5 freeze
click==6.7
Flask==0.12.2
itsdangerous==0.24
Jinja2==2.10
MarkupSafe==1.0
style==1.1.0
update==0.0.1
Werkzeug==0.14.1